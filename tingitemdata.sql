/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : tingitemdata

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 15/10/2021 10:39:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_admin
-- ----------------------------
DROP TABLE IF EXISTS `t_admin`;
CREATE TABLE `t_admin`  (
  `aid` int NOT NULL AUTO_INCREMENT COMMENT '管理员编号',
  `aname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '管理员姓名',
  `apwd` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '管理员密码',
  `aphone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '管理员手机号码',
  `starttime` datetime NOT NULL COMMENT '开通时间',
  PRIMARY KEY (`aid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_admin
-- ----------------------------
INSERT INTO `t_admin` VALUES (1, 'admin', '123', '13812345678', '2021-08-12 10:39:45');

-- ----------------------------
-- Table structure for t_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `t_admin_role`;
CREATE TABLE `t_admin_role`  (
  `aid` int NOT NULL COMMENT '管理员编号',
  `rid` int NOT NULL COMMENT '角色编号'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_admin_role
-- ----------------------------
INSERT INTO `t_admin_role` VALUES (1, 1);

-- ----------------------------
-- Table structure for t_company
-- ----------------------------
DROP TABLE IF EXISTS `t_company`;
CREATE TABLE `t_company`  (
  `cid` int NOT NULL AUTO_INCREMENT COMMENT '公司编号',
  `cpwd` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '登录密码',
  `cname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公司名称',
  `ceo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公司法人',
  `cmail` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公司邮箱',
  `starttime` datetime NOT NULL COMMENT '注册时间',
  `status` char(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账号状态(正常，禁用，未审核)',
  `ordernumber` int NULL DEFAULT NULL COMMENT '公司订单数量',
  PRIMARY KEY (`cid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_company
-- ----------------------------

-- ----------------------------
-- Table structure for t_host
-- ----------------------------
DROP TABLE IF EXISTS `t_host`;
CREATE TABLE `t_host`  (
  `hid` int NOT NULL AUTO_INCREMENT COMMENT '主持人编号',
  `hname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主持人姓名',
  `hpwd` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主持人密码',
  `hphone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主持人手机号码',
  `starttime` datetime NOT NULL COMMENT '开始时间',
  `status` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账号状态(正常，禁用)',
  `strong` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '权重',
  `ordernumber` int NULL DEFAULT NULL COMMENT '主持人订单数量',
  PRIMARY KEY (`hid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_host
-- ----------------------------
INSERT INTO `t_host` VALUES (1, '刘星星', '123', '13812312312', '2021-08-18 20:17:09', '0', '90', 19);
INSERT INTO `t_host` VALUES (2, '张三三', '123', '13812312312', '2021-08-18 20:17:09', '0', '10', 22);
INSERT INTO `t_host` VALUES (3, '刘思思', '123', '13812312312', '2021-08-18 20:17:09', '0', '30', 30);
INSERT INTO `t_host` VALUES (4, '王呜呜', '123', '13812312312', '2021-08-18 20:17:09', '0', '40', 40);
INSERT INTO `t_host` VALUES (5, '王思思', '123', '13812312312', '2021-08-18 20:17:09', '0', '40', 50);
INSERT INTO `t_host` VALUES (6, '李洒洒', '123', '13812312312', '2021-08-18 20:17:09', '0', '30', 0);
INSERT INTO `t_host` VALUES (7, '周六六', '123', '13812312312', '2021-08-18 20:17:09', '1', '50', 0);
INSERT INTO `t_host` VALUES (8, '找三三', '123', '13812312312', '2021-08-18 20:17:09', '1', '60', 0);
INSERT INTO `t_host` VALUES (9, '张其', '123', '13812312312', '2021-08-18 20:17:09', '1', '60', 0);
INSERT INTO `t_host` VALUES (10, '美滋滋', '123', '13812312312', '2021-08-18 20:17:09', '1', '60', 0);
INSERT INTO `t_host` VALUES (11, '张二二', '123', '13812312312', '2021-08-18 20:17:09', '1', '60', 0);
INSERT INTO `t_host` VALUES (12, '玉龙', '123', '123123123', '2021-08-30 10:51:56', '1', '50', 0);
INSERT INTO `t_host` VALUES (13, '刘雨龙', '123123', '11111111', '2021-08-30 10:54:29', '1', '50', 0);
INSERT INTO `t_host` VALUES (14, '玉龙', '11111', '1111111', '2021-08-30 11:00:53', '1', '50', 0);
INSERT INTO `t_host` VALUES (15, '123123', '123', '', '2021-08-30 16:11:44', '0', '50', 0);
INSERT INTO `t_host` VALUES (16, '玉龙', '111', '11111', '2021-08-30 16:45:53', '1', '50', 0);
INSERT INTO `t_host` VALUES (17, '111111111', '111111', '1111111111', '2021-08-30 17:23:28', '0', '50', 0);

-- ----------------------------
-- Table structure for t_host_power
-- ----------------------------
DROP TABLE IF EXISTS `t_host_power`;
CREATE TABLE `t_host_power`  (
  `hpid` int NOT NULL AUTO_INCREMENT COMMENT '权限编号',
  `hpstar` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否星推荐',
  `hpstar_begindate` date NULL DEFAULT NULL COMMENT '星推荐开始日期',
  `hpstar_enddate` date NULL DEFAULT NULL COMMENT '星推荐结束日期',
  `hp_order_power` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否允许自添加订单',
  `hpstar_begintime` time NULL DEFAULT NULL COMMENT '每日星推荐开始时间',
  `hpstar_endtime` time NULL DEFAULT NULL COMMENT '每日星推荐结束时间',
  `hpdiscount` int NULL DEFAULT NULL COMMENT '折扣值(6,7,8,9)',
  `hp_dis_starttime` date NULL DEFAULT NULL COMMENT '折扣开始时间',
  `hp_dis_endtime` date NULL DEFAULT NULL COMMENT '折扣借宿时间',
  `hpprice` double NOT NULL COMMENT '价格',
  `hpcosts` double NOT NULL COMMENT '管理费',
  `hostid` int NOT NULL COMMENT '主持人编号',
  PRIMARY KEY (`hpid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_host_power
-- ----------------------------
INSERT INTO `t_host_power` VALUES (1, '1', '2021-08-19', '2021-09-01', '1', '08:30:00', '18:30:00', 9, '2021-08-19', '2021-09-01', 5000, 2000, 1);
INSERT INTO `t_host_power` VALUES (2, '1', '2021-09-15', '2021-09-15', '1', '03:00:00', '21:00:00', 9, '2021-09-01', '2021-09-22', 111, 111, 2);
INSERT INTO `t_host_power` VALUES (3, '1', '2021-09-08', '2021-09-09', '1', '21:00:00', '03:00:00', 7, '2021-09-08', '2021-09-09', 111, 111, 3);
INSERT INTO `t_host_power` VALUES (4, '1', '2021-08-19', '2021-09-01', '1', '08:30:00', '18:30:00', 6, '2021-08-25', '2021-09-01', 2000, 1000, 4);
INSERT INTO `t_host_power` VALUES (5, '1', '2021-08-19', '2021-09-01', '1', '08:30:00', '18:30:00', 6, '2021-08-23', '2021-09-02', 2000, 1000, 5);
INSERT INTO `t_host_power` VALUES (6, '1', '2021-08-19', '2021-09-01', '1', '08:30:00', '18:30:00', 6, '2021-08-18', '2021-09-03', 2000, 1000, 6);
INSERT INTO `t_host_power` VALUES (7, '1', '2021-08-19', '2021-09-01', '1', '08:30:00', '18:30:00', 6, '2021-08-12', '2021-09-04', 2000, 1000, 7);
INSERT INTO `t_host_power` VALUES (8, '1', '2021-08-19', '2021-09-01', '1', '08:30:00', '18:30:00', 9, '2021-08-29', '2021-09-05', 2000, 2000, 8);
INSERT INTO `t_host_power` VALUES (9, '1', '2021-09-08', '2021-10-01', '1', '10:40:00', '18:30:00', 7, '2021-09-08', '2021-09-10', 999, 999, 12);
INSERT INTO `t_host_power` VALUES (10, '0', '2021-09-08', '2021-09-24', '1', '07:00:00', '04:00:00', 7, '2021-09-09', '2021-09-15', 111, 111, 14);
INSERT INTO `t_host_power` VALUES (15, '1', '2021-09-08', '2021-09-09', '1', '21:00:00', '03:00:00', 9, '2021-09-08', '2021-09-09', 999, 999, 17);
INSERT INTO `t_host_power` VALUES (16, '1', '2021-09-08', '2021-09-09', '1', '21:00:00', '03:00:00', 9, '2021-09-08', '2021-09-09', 999, 999, 15);

-- ----------------------------
-- Table structure for t_married_person
-- ----------------------------
DROP TABLE IF EXISTS `t_married_person`;
CREATE TABLE `t_married_person`  (
  `pid` int NOT NULL AUTO_INCREMENT COMMENT '新人ID',
  `ppwd` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '新人网站密码',
  `pname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '新人姓名',
  `phone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '手机号',
  `pmail` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '邮箱号',
  `marrydate` date NOT NULL COMMENT '婚期',
  `regdate` datetime NOT NULL COMMENT '注册时间',
  `status` char(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账号状态（正常，禁用）',
  PRIMARY KEY (`pid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_married_person
-- ----------------------------

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu`  (
  `mid` int NOT NULL AUTO_INCREMENT COMMENT '菜单编号',
  `mname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单名',
  `pid` int NOT NULL COMMENT '父菜单ID',
  `isparent` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '是否为父级菜单(1.是,0.否)',
  `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '打开状态(1.展开,0.不展开)',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'url地址',
  `mdesc` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单描述',
  PRIMARY KEY (`mid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES (1, '成员管理', 0, '1', '0', NULL, '一级菜单');
INSERT INTO `t_menu` VALUES (2, '成员订单中心', 0, '1', '0', NULL, '一级菜单');
INSERT INTO `t_menu` VALUES (3, '内容管理', 0, '1', '0', NULL, '一级菜单');
INSERT INTO `t_menu` VALUES (4, '其他管理', 0, '1', '0', NULL, '一级菜单');
INSERT INTO `t_menu` VALUES (5, '主持人管理', 1, '0', '0', 'memenber/zcrManage.jsp', '二级菜单');
INSERT INTO `t_menu` VALUES (6, '婚庆公司管理', 1, '0', '0', 'memenber/hqManage.jsp', '二级菜单');
INSERT INTO `t_menu` VALUES (7, '新人管理', 1, '0', '0', 'memenber/xrManage.jsp', '二级菜单');
INSERT INTO `t_menu` VALUES (8, '主持人订单中心', 2, '0', '0', 'memenber/error.jsp', '二级菜单');
INSERT INTO `t_menu` VALUES (9, 'banner管理', 3, '0', '0', 'memenber/error.jsp', '二级菜单');
INSERT INTO `t_menu` VALUES (10, '首页推荐管理', 3, '0', '0', 'memenber/error.jsp', '二级菜单');
INSERT INTO `t_menu` VALUES (11, '底部文案、二维码设置', 3, '0', '0', 'memenber/error.jsp', '二级菜单');
INSERT INTO `t_menu` VALUES (12, '加入团队介绍文案', 3, '0', '0', 'memenber/error.jsp', '二级菜单');
INSERT INTO `t_menu` VALUES (13, '关于团队文案', 3, '0', '0', 'memenber/error.jsp', '二级菜单');
INSERT INTO `t_menu` VALUES (14, '菜单管理', 4, '0', '0', 'otherManager/menuManager.jsp', '二级菜单');
INSERT INTO `t_menu` VALUES (15, '角色管理', 4, '0', '0', 'otherManager/roleManager.jsp', '二级菜单');
INSERT INTO `t_menu` VALUES (16, '管理员管理', 4, '0', '0', 'memenber/error.jsp', '二级菜单');

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order`  (
  `oid` int NOT NULL COMMENT '订单id',
  `pid` int NOT NULL COMMENT '新人id',
  `cid` int NOT NULL COMMENT '婚庆公司id',
  `hid` int NOT NULL COMMENT '主持人id',
  `hotelname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '酒店名',
  `hoteladdress` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '酒店地址',
  `ordertime` datetime NOT NULL COMMENT '下单时间',
  `wedding_time` datetime NOT NULL COMMENT '结婚时间',
  `wedding_split` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '离婚',
  `deposit` double NOT NULL COMMENT '定金',
  `money` double NOT NULL COMMENT '金额',
  `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '完成状态',
  `comment` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单注释'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_order
-- ----------------------------

-- ----------------------------
-- Table structure for t_planner
-- ----------------------------
DROP TABLE IF EXISTS `t_planner`;
CREATE TABLE `t_planner`  (
  `nid` int NOT NULL AUTO_INCREMENT COMMENT '策划师编号',
  `nname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '策划师姓名',
  `nphone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '策划师手机号',
  `addtime` datetime NOT NULL COMMENT '添加时间',
  `status` char(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账号状态(正常，禁用)',
  `cid` int NOT NULL COMMENT '公司id',
  `ordernumber` int NULL DEFAULT NULL COMMENT '策划师订单数量',
  PRIMARY KEY (`nid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_planner
-- ----------------------------

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role`  (
  `rid` int NOT NULL AUTO_INCREMENT COMMENT '角色编号',
  `rname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名称',
  `rdesc` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色描述',
  PRIMARY KEY (`rid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role
-- ----------------------------

-- ----------------------------
-- Table structure for t_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_role_menu`;
CREATE TABLE `t_role_menu`  (
  `rid` int NOT NULL COMMENT '角色编号',
  `mid` int NOT NULL COMMENT '菜单编号'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role_menu
-- ----------------------------
INSERT INTO `t_role_menu` VALUES (1, 1);
INSERT INTO `t_role_menu` VALUES (1, 2);
INSERT INTO `t_role_menu` VALUES (1, 6);
INSERT INTO `t_role_menu` VALUES (1, 7);
INSERT INTO `t_role_menu` VALUES (1, 4);
INSERT INTO `t_role_menu` VALUES (1, 5);
INSERT INTO `t_role_menu` VALUES (1, 8);
INSERT INTO `t_role_menu` VALUES (1, 13);
INSERT INTO `t_role_menu` VALUES (1, 10);
INSERT INTO `t_role_menu` VALUES (1, 11);
INSERT INTO `t_role_menu` VALUES (1, 12);
INSERT INTO `t_role_menu` VALUES (1, 14);
INSERT INTO `t_role_menu` VALUES (1, 15);
INSERT INTO `t_role_menu` VALUES (1, 16);
INSERT INTO `t_role_menu` VALUES (1, 3);

SET FOREIGN_KEY_CHECKS = 1;
