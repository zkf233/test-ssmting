package com.lyl.util;

import com.lyl.pojo.Host;
import com.lyl.pojo.HostJSON;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ToolUtil {
    public static void download(HttpServletResponse resp,String fileName,String path){
        try {
            //设置响应头，控制浏览器下载该文件
            resp.setHeader("content-disposition", "attachment;fileName=" + URLEncoder.encode(fileName, "UTF-8"));
            //读取要下载的文件，保存到文件输入流
            FileInputStream in = new FileInputStream(path + "\\" + fileName);
            //创建输出流
            OutputStream out = resp.getOutputStream();
            //创建缓冲区
            byte[] buffer = new byte[1024];
            int len = 0;
            //循环将输入流中的内容读取到缓冲区当中
            while((len=in.read(buffer))>0){
                //输出缓冲区的内容到浏览器，实现文件下载
                out.write(buffer, 0, len);
            }
            //关闭文件输入流
            in.close();
            //关闭输出流
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 导出Host信息到exc表格中
     * @param list 主持人实体信息
     */
    public static void exportExc(List<Host> list, HttpServletRequest request,String fileName) {
        // 1.创建XSSFWorkbook对象得到XSSFSheet来操作exc表
        XSSFWorkbook xssfWorkbook = new XSSFWorkbook();
        // 2.创建工作表对象
        XSSFSheet sheet1 = xssfWorkbook.createSheet("Sheet1");
        // 3.创建row有几条数据就创建几条row，第一条为字段名，需要手动设置
        XSSFRow rowTitle = sheet1.createRow(0);
        // 设置标题
        String[] titles = {"权重", "姓名", "手机号码", "开通时间", "价格", "订单量", "折扣", "星推荐", "帐号状态"};
        for (int i = 0; i < titles.length; i++) {
            rowTitle.createCell(i).setCellValue(titles[i]);
        }
        // 开始导入Host信息
        for (int i = 1; i <= list.size(); i++) {
            XSSFRow row = sheet1.createRow(i);
            // 得到Host对象
            Host host = list.get(i - 1);
            String[] values = {host.getStrong(),
                    host.getHname(),
                    host.getHphone(),
                    dateToString(host.getStarttime()),
                    host.getHostPower()==null?null:host.getHostPower().getHpprice()+"",
                    host.getOrdernumber()+"",
                    host.getHostPower()==null?null:host.getHostPower().getHpdiscount()+"",
                    host.getHostPower()==null?null:host.getHostPower().getHpstar().equals("1")?"是":"否",
                    host.getStatus().equals("1")?"正常":"异常"};
            for (int j = 0; j < titles.length; j++) {
                row.createCell(j).setCellValue(values[j]);
            }
        }
        // 由于是Ajax请求，所以返回的都是字符流，我们采用将其文件下载到服务器再对外提供文件名供外部浏览器下载
        // 文件输出
        BufferedOutputStream bufferedOutput = null;
        try {

            // 获取path
            String path = request.getSession().getServletContext().getRealPath("/WEB-INF/upload");
            File tmpFile = new File(path);
            if (!tmpFile.exists()) {
                //创建临时目录
                if (tmpFile.mkdir()) {
                    System.out.println(tmpFile);
                }
            }
            // 文件全路径
            String fileUrl = path+"\\"+fileName+".xls";
            FileOutputStream fileOutputStream = new FileOutputStream(fileUrl);
            bufferedOutput = new BufferedOutputStream(fileOutputStream);
            bufferedOutput.flush();// 刷新缓存区
            xssfWorkbook.write(bufferedOutput);// 将数据写入bufferedOutput
            // 文件已经保存在服务器中，只需要传递给ajax路径，让浏览器直接下载即可


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 释放资源
            if(bufferedOutput!=null){
                try {
                    bufferedOutput.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                xssfWorkbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 格式化Date对象
     * @param date 需要被格式化的Date
     * @return 被格式化String的Date
     */
    public static String dateToString(Date date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }
}
