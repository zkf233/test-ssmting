package com.lyl.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@TableName("t_company")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Company extends Model<Company> {

    private static final long serialVersionUID=1L;

    /**
     * 公司编号
     */
      @TableId(value = "cid", type = IdType.AUTO)
    private Integer cid;

    /**
     * 登录密码
     */
    private String cpwd;

    /**
     * 公司名称
     */
    private String cname;

    /**
     * 公司法人
     */
    private String ceo;

    /**
     * 公司邮箱
     */
    private String cmail;

    /**
     * 注册时间
     */
    private Date starttime;

    /**
     * 账号状态(正常，禁用，未审核)
     */
    private String status;

    /**
     * 公司订单数量
     */
    private Integer ordernumber;

    @Override
    protected Serializable pkVal() {
        return this.cid;
    }

}
