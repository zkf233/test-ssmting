package com.lyl.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */

@EqualsAndHashCode(callSuper = true)
@TableName("t_admin")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Admin extends Model<Admin> {

    private static final long serialVersionUID=1L;

    /**
     * 管理员编号
     */
      @TableId(value = "aid", type = IdType.AUTO)
    private Integer aid;

    /**
     * 管理员姓名
     */
    private String aname;

    /**
     * 管理员密码
     */
    private String apwd;

    /**
     * 管理员手机号码
     */
    private String aphone;

    /**
     * 开通时间
     */
    private Date starttime;

    @Override
    protected Serializable pkVal() {
        return this.aid;
    }

}
