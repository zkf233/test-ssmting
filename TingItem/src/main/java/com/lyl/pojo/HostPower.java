package com.lyl.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@TableName("t_host_power")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class HostPower extends Model<HostPower> {

    private static final long serialVersionUID=1L;

    /**
     * 权限编号
     */
      @TableId(value = "hpid", type = IdType.AUTO)
    private Integer hpid;

    /**
     * 是否星推荐
     */
    private String hpstar;

    /**
     * 星推荐开始日期
     * Spring的日期格式化，前端传入的日期格式是字符串，没办法直接强转，需要借助 @DateTimeFormat注释并规定格式
     * Spring会将其转型
     */
    @DateTimeFormat( pattern = "yyyy-MM-dd")
    private Date hpstarBegindate;

    /**
     * 星推荐结束日期
     */
    @DateTimeFormat( pattern = "yyyy-MM-dd")
    private Date hpstarEnddate;

    /**
     * 是否允许自添加订单
     */
    private String hpOrderPower;

    /**
     * 每日星推荐开始时间
     */
    @DateTimeFormat( pattern = "HH:mm:ss")
    private Date hpstarBegintime;

    /**
     * 每日星推荐结束时间
     */
    @DateTimeFormat( pattern = "HH:mm:ss")
    private Date hpstarEndtime;

    /**
     * 折扣值(6,7,8,9)
     */
    private Integer hpdiscount;

    /**
     * 折扣开始时间
     */
    @DateTimeFormat( pattern = "yyyy-MM-dd")
    private Date hpDisStarttime;

    /**
     * 折扣借宿时间
     */
    @DateTimeFormat( pattern = "yyyy-MM-dd")
    private Date hpDisEndtime;

    /**
     * 价格
     */
    private Double hpprice;

    /**
     * 管理费
     */
    private Double hpcosts;

    /**
     * 主持人编号
     */
    private Integer hostid;

    @Override
    protected Serializable pkVal() {
        return this.hpid;
    }

}
