package com.lyl.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@TableName("t_order")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Order extends Model<Order> {

    private static final long serialVersionUID=1L;

    /**
     * 订单id
     */
    private Integer oid;

    /**
     * 新人id
     */
    private Integer pid;

    /**
     * 婚庆公司id
     */
    private Integer cid;

    /**
     * 主持人id
     */
    private Integer hid;

    /**
     * 酒店名
     */
    private String hotelname;

    /**
     * 酒店地址
     */
    private String hoteladdress;

    /**
     * 下单时间
     */
    private Date ordertime;

    /**
     * 结婚时间
     */
    private Date weddingTime;

    /**
     * 离婚
     */
    private String weddingSplit;

    /**
     * 定金
     */
    private Double deposit;

    /**
     * 金额
     */
    private Double money;

    /**
     * 完成状态
     */
    private String status;

    /**
     * 订单注释
     */
    private String comment;

    @Override
    protected Serializable pkVal() {
        return null;
    }

}
