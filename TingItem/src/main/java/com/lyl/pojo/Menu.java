package com.lyl.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@TableName("t_menu")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Menu extends Model<Menu> {

    private static final long serialVersionUID=1L;

    /**
     * 菜单编号
     */
      @TableId(value = "mid", type = IdType.AUTO)
    private Integer mid;

    /**
     * 菜单名
     */
    private String mname;

    /**
     * 父菜单ID
     */
    private Integer pid;

    /**
     * 是否为父级菜单(1.是,0.否)
     */
    private String isparent;

    /**
     * 打开状态(1.展开,0.不展开)
     */
    private String status;

    /**
     * url地址
     */
    private String url;

    /**
     * 菜单描述
     */
    private String mdesc;

    @Override
    protected Serializable pkVal() {
        return this.mid;
    }

}
