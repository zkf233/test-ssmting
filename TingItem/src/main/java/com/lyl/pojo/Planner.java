package com.lyl.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@TableName("t_planner")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Planner extends Model<Planner> {

    private static final long serialVersionUID=1L;

    /**
     * 策划师编号
     */
      @TableId(value = "nid", type = IdType.AUTO)
    private Integer nid;

    /**
     * 策划师姓名
     */
    private String nname;

    /**
     * 策划师手机号
     */
    private String nphone;

    /**
     * 添加时间
     */
    private Date addtime;

    /**
     * 账号状态(正常，禁用)
     */
    private String status;

    /**
     * 公司id
     */
    private Integer cid;

    /**
     * 策划师订单数量
     */
    private Integer ordernumber;

    @Override
    protected Serializable pkVal() {
        return this.nid;
    }

}
