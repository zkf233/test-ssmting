package com.lyl.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@TableName("t_role_menu")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RoleMenu extends Model<RoleMenu> {

    private static final long serialVersionUID=1L;

    /**
     * 角色编号
     */
    private Integer rid;

    /**
     * 菜单编号
     */
    private Integer mid;

    @Override
    protected Serializable pkVal() {
        return null;
    }

}
