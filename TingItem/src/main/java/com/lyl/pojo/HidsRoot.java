package com.lyl.pojo;

import java.util.List;

public class HidsRoot {
    private List<Integer> ids;

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }

    public List<Integer> getIds() {
        return ids;
    }
}
