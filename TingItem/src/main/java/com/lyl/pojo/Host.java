package com.lyl.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@EqualsAndHashCode(callSuper = true)
@TableName("t_host")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Host extends Model<Host> {

    private static final long serialVersionUID=1L;



    /**
     * 主持人编号
     */
      @TableId(value = "hid", type = IdType.AUTO)
    private Integer hid;

    /**
     * 主持人姓名
     */
    private String hname;


    /**
     * 主持人密码
     */
    private String hpwd;

    /**
     * 主持人手机号码
     */
    private String hphone;

    /**
     * 开始时间
     */
    private Date starttime;

    /**
     * 账号状态(正常，禁用)
     */
    private String status;

    /**
     * 权重
     */
    private String strong;

    /**
     * 主持人订单数量
     */
    private Integer ordernumber;

    /**
     * 主持人权限信息，告诉MP该属性非数据库字段属性
     */

    @TableField(exist = false)
    private HostPower hostPower;

    @Override
    protected Serializable pkVal() {
        return this.hid;
    }

}
