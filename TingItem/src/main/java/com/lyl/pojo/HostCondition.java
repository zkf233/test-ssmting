package com.lyl.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class HostCondition {
    private String hname;
    private String status;
    private String strong;
    private String hpstar;
    private String hpdiscount;
}
