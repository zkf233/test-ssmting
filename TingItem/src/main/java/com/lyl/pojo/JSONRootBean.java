package com.lyl.pojo;

import java.util.List;

public class JSONRootBean {
    private List<Host> hosts;
    public void setHosts(List<Host> hosts) {
        this.hosts = hosts;
    }
    public List<Host> getHosts() {
        return hosts;
    }
}
