package com.lyl.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@TableName("t_married_person")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class MarriedPerson extends Model<MarriedPerson> {

    private static final long serialVersionUID=1L;

    /**
     * 新人ID
     */
      @TableId(value = "pid", type = IdType.AUTO)
    private Integer pid;

    /**
     * 新人网站密码
     */
    private String ppwd;

    /**
     * 新人姓名
     */
    private String pname;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 邮箱号
     */
    private String pmail;

    /**
     * 婚期
     */
    private Date marrydate;

    /**
     * 注册时间
     */
    private Date regdate;

    /**
     * 账号状态（正常，禁用）
     */
    private String status;

    @Override
    protected Serializable pkVal() {
        return this.pid;
    }

}
