package com.lyl.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@TableName("t_role")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Role extends Model<Role> {

    private static final long serialVersionUID=1L;

    /**
     * 角色编号
     */
      @TableId(value = "rid", type = IdType.AUTO)
    private Integer rid;

    /**
     * 角色名称
     */
    private String rname;

    /**
     * 角色描述
     */
    private String rdesc;

    @Override
    protected Serializable pkVal() {
        return this.rid;
    }

}
