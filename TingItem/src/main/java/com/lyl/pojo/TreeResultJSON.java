package com.lyl.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * 菜单加载异步树：
 * 前台需要的是JSON格式，所以在后台需要创建一个前台接收的JSON格式模板
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TreeResultJSON {
    private Integer id;
    private String text;
    private String state;
    private Map<String,Object> attributes;
}
