package com.lyl.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lyl.pojo.Host;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyl.pojo.HostCondition;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
public interface HostMapper extends BaseMapper<Host> {
    IPage<Host> selectHostInfo(IPage<Host> page, @Param("hostCondition") HostCondition hostCondition);
}
