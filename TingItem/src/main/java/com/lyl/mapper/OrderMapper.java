package com.lyl.mapper;

import com.lyl.pojo.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
public interface OrderMapper extends BaseMapper<Order> {

}
