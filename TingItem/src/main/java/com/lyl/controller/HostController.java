package com.lyl.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lyl.pojo.*;
import com.lyl.service.HostService;
import com.lyl.util.ToolUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@Controller
@RequestMapping("/host")
public class HostController {
    // 注入业务层
    private HostService hostService;

    @Autowired
    public void setHostService(HostService hostService) {
        this.hostService = hostService;
    }

    // 分页加载主持人信息

    /**
     * @param rows          每页显示数据
     * @param page          页码数
     * @param hostCondition 接收查询条件参数
     */
    @ResponseBody
    @RequestMapping("hostInfo")
    public HostJSON<Host> hostInfo(Long rows, Long page, HostCondition hostCondition) {
        System.out.println("*********************************************************");
        HostJSON<Host> hostJSON = new HostJSON<>();
        IPage<Host> page1 = new Page<>(page, rows);
        System.out.println(hostCondition);
        IPage<Host> list = hostService.findHostInfo(page1, hostCondition);
        // 将list的数据集中到hostJSON
        hostJSON.setTotal(list.getTotal());
        System.out.println(list.getTotal());
        hostJSON.setRows(list.getRecords());
        return hostJSON;
    }

    /**
     * 导出功能
     * <p>
     * public class JSONBean{
     * private List<Host> hosts;
     * public void setHosts(List<Host> hosts) {
     * this.hosts = hosts;
     * }
     * public List<Host> getHosts() {
     * return hosts;
     * }
     * <p>
     * 这样方便我们直接送bean中提取我们的数据，简单高效，针对含有List或者Map的形式数据
     */

    @ResponseBody
    @RequestMapping("export")
    public TreeResultJSON exportExc(@RequestBody String hosts, HttpServletRequest request) {
        TreeResultJSON resultJSON = new TreeResultJSON();
        String fileName = "exc";
        try {
            //1. 解析数据
            //2. 前台数据以JSON格式的String形式传递过来，我们需要借助json工具包来解析出JSON格式
            ObjectMapper objectMapper = new ObjectMapper();
            //3. 由于前台传递的是Array类型的字符串数据，为防止反序列化时出现不能反序列化List，我们将Array中的单个元素类型封装到一个JSONBean中
            JSONRootBean list = objectMapper.readValue(hosts, JSONRootBean.class);
            System.out.println(list);
            //4. 开始进行导出操作，从bean中取出List<Host>集合数据，使用POI包进行导出操作
            List<Host> hosts1 = list.getHosts();
            for (Host host : hosts1) {
                System.out.println(host);
            }
            ToolUtil.exportExc(hosts1, request, fileName);
            System.out.println("导出文件名：" + fileName);
            resultJSON.setText(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultJSON;
    }

    @RequestMapping("download")
    public void download(String fileName, HttpServletResponse resp, HttpServletRequest request) {
        ToolUtil.download(resp, fileName, request.getSession().getServletContext().getRealPath("/WEB-INF/upload"));
    }

    /*
     * 添加主持人
     */

    @RequestMapping("addHost")
    @ResponseBody
    public Result insertHost(Host host) {
        System.out.println(host);
        // 添加替他数据
        // 添加开始时间
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        host.setStarttime(timestamp);
        String status = "1";
        String strong = "50";
        Integer ordernumber = 0;
        host.setStatus(status);
        host.setStrong(strong);
        host.setOrdernumber(ordernumber);
        System.out.println(host);
        boolean insert = host.insert();
        return new Result(insert, insert ? "新增成功！" : "新增失败");
    }

    /*
     * 账号禁用操作
     */
    @ResponseBody
    @RequestMapping("stopUser")
    public Result stopUser(@RequestBody String hosts) {
        ObjectMapper objectMapper = new ObjectMapper();
        System.out.println(hosts);
        boolean b = false;
        //3. 由于前台传递的是Array类型的字符串数据，为防止反序列化时出现不能反序列化List，我们将Array中的单个元素类型封装到一个JSONBean中
        try {
            JSONRootBean jsonRootBean = objectMapper.readValue(hosts, JSONRootBean.class);
            System.out.println(jsonRootBean);
            List<Host> list = jsonRootBean.getHosts();
            for (Host host : list) {
                host.setStatus(host.getStatus().equals("1")?"0":"1");
            }
            b = hostService.updateBatchById(list);

            System.out.println(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result(b, b ? "修改账户状态成功！" : "修改账户状态失败");
    }
    // 批量删除
    /*
    传递host的hid给后台，后台对其进行删除，批量删除
     */
    @RequestMapping("deleteHost")
    @ResponseBody
    public Result deleteHost(){

        return null;
    }
    // 权限修改
    /*
    传递选中的Host对象给后台，
     */
    @RequestMapping("updateHost")
    @ResponseBody
    public Result upDateHost(){

        return null;
    }

    // 修改权重
    @RequestMapping("strongUp")
    @ResponseBody
    public Result StrongUpData(Host host){
        boolean b = host.updateById();
        return new Result(b,b?"权重修改成功！":"权重修改失败！");
    }

}

