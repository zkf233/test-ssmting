package com.lyl.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lyl.pojo.Admin;
import com.lyl.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpSession;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@Controller
@RequestMapping("/admin")
public class AdminController {
    // 声明对应的业务层属性
    private AdminService adminService;

    @Autowired
    public void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }

    @RequestMapping("userLogin")
    public String userLogin(String username, String password, HttpSession session){
        System.out.println("username:"+username);
        System.out.println("password:"+password);
        System.out.println(adminService);
        // 创建条件构造器
        QueryWrapper<Admin> wrapper = new QueryWrapper<>();
        wrapper.eq("aname",username).eq("apwd",password);
        // 利用业务层给与的方法直接调用
        Admin admin = adminService.getOne(wrapper);
        System.out.println(admin);
        // 判断admin是否存在
        if(admin!=null){
            // 存在则将admin对象放入session保存
            session.setAttribute("admin",admin);
            return "redirect:/main.jsp";
        }
        session.setAttribute("flag","loginNo");
        return "redirect:/login.jsp";
    }
}

