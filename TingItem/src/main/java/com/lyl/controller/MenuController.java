package com.lyl.controller;


import com.lyl.pojo.Admin;
import com.lyl.pojo.TreeResultJSON;
import com.lyl.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@Controller
@RequestMapping("/menu")
public class MenuController {

    private MenuService menuService;

    @Autowired
    public void setMenuService(MenuService menuService) {
        this.menuService = menuService;
    }

    /**
     *异步树菜单实现的原理：
     * 1。由EasyUI给我们封装好的异步树菜单控件直接使用。可以查看官方API
     * 2.搞清楚异步树菜单与后台交互的原理：
     *  最关键的就是id属性，id将会作为http的参数并命名为‘id’传递给服务器，让服务器根据节点id检索子节点，这在经过rbac设计的数据库表中的，t_menu表中对应的是mid，
     *  而pid则作为依赖于父节点的id，也就是规定的父节点所拥有的子节点id。
     *
     *  最终异步树菜单实现效果为，页面加载时，自动发送Ajax请求加载菜单，节点A（id=1）与节点B(id=2)如果都未被展开，点击节点A，则会向服务器发送Ajax请求，id=1的数据，
     *  服务器接收后则会在对应的方法中查询最后返回pid = 1的菜单信息，因为所有pid=1的结果都隶属于节点A下的子节点
     *  3.编写方法：
     *      a。根据EasyUI的要求，我们返回的必须是固定JSON格式，并根据API提供的模板，在pojo中创建JSON模板类，用于存放返回给浏览器端的JSON数据
     *      b。我们不再在Mapper层中的xml文件中书写sql语句，我们直接使用MP给我们提供的Wrapper，使用面向对象的方式写sql操作
     *      c。理解RBAC的设计原理，弄清楚角色与权限的关系
     *      d。弄清楚mid与pid：
     *          mid是菜单的id，每一个菜单都会有自己id，但是有一些是目录，肯定就包含了子目录，所以他们的pid就代表着该菜单元素属于哪一个mid的子目录
     *          例如：一级菜单为一开始就必须显示的菜单目录，所以他们的pid一般为0，代表着一开始就要加载出来的。
     *              二级菜单为一级菜单的子菜单，所以他们的pid就必须附属于一级菜单的mid，多个pid等于一个mid的值，则代表着这些二级菜单都是归mid的一级菜单管理
     *              所以异步树在发送id的Ajax请求时，实际上就是给pid赋值，找到那些子菜单
     *
     *              aid又是什么？根据aid查询到对应的角色--》rid
     *              根据rid我们可以查询到rid对应的菜单--》mid
     *              在根据mid我们就可以查询到菜单信息，但是查询出来的是所有菜单信息，而我们要查询的是父目录的子目录菜单信息，
     *              所以加上pid的限制，就可以在符合aid所有的菜单信息中，找到父目录需要的菜单信息
     */
    @ResponseBody // 会将对象返回到Http Response body中，以JSON的格式
    @RequestMapping("getMenu")
    // 为防止第一次加载出错设置默认id加载一级菜单
    // admin 拥有所有权限
    public List<TreeResultJSON> getMenu(@RequestParam(defaultValue = "0") String id, HttpSession session){
        // 得到admin的id
        Admin admin = (Admin) session.getAttribute("admin");
        Integer aid = admin.getAid();
        // 调用业务层方法查询菜单内容，该方法需要自己手动写
        //
        return menuService.selMenuInfo(id,aid);
    }
}

