package com.lyl.controller;

import com.lyl.pojo.HostPower;
import com.lyl.pojo.Result;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.Arrays;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@Controller
@RequestMapping("/hostPower")
public class HostPowerController {

    // 修改权限，接受Host对象，将Host对象的hostPower属性传递到HostPowerController进行数据添加
    @RequestMapping("power")
    @ResponseBody
    public Result setPower(HostPower hostPower){
        System.out.println(hostPower);
        // insertOrUpdate()方法内部帮我们做了主键是否为空判断
        boolean b = hostPower.insertOrUpdate();
        return new Result(b,b?"权限修改成功！":"权限修改失败！");
    }

    /**
     * 批量修改主持人权限
     * @param hostPower 存储通用主持人权限设置数据
     * @return 状态
     */
    @RequestMapping("batchHostPower")
    @ResponseBody
    public Result batchHostPower(String hostids,HostPower hostPower,String pids){
        String[] hostidList = hostids.split(",");
        String[] pidList = pids.split(",");
        System.out.println(Arrays.toString(hostidList));
        System.out.println(Arrays.toString(pidList));
        // 对比两个组，根据前台传递参数的规则，hostidList与pidList的数据一一对应，所以，如果hostid有对应的pid则进行修改操作
        // 如果没有对应的pid则进行插入操作
        for (int i = 0; i < pidList.length; i++) {
            if (!pidList[i].equals(" ")){
                // 进行修改操作，找出对应的hpid即可
                hostPower.setHpid(Integer.parseInt(pidList[i]));
            }
            // 每个操作都hostid
            hostPower.setHostid(Integer.parseInt(hostidList[i]));
            System.out.println(hostPower);
            hostPower.insertOrUpdate();
            // hpid重置，因为hostid不可能为null所以不需要重置，每一次循环的值都不一样
            hostPower.setHpid(null);
        }
        return new Result(true, "批量修改成功！");
    }
}

