package com.lyl.service.impl;

import com.lyl.pojo.HostPower;
import com.lyl.mapper.HostPowerMapper;
import com.lyl.service.HostPowerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@Service
public class HostPowerServiceImpl extends ServiceImpl<HostPowerMapper, HostPower> implements HostPowerService {

}
