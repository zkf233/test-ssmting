package com.lyl.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lyl.pojo.Host;
import com.lyl.mapper.HostMapper;
import com.lyl.pojo.HostCondition;
import com.lyl.service.HostService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@Service
public class HostServiceImpl extends ServiceImpl<HostMapper, Host> implements HostService {
    private HostMapper hostMapper;

    @Autowired
    public void setHostMapper(HostMapper hostMapper) {

        this.hostMapper = hostMapper;
    }

    @Override
    public IPage<Host> findHostInfo(IPage<Host> page, HostCondition hostCondition) {
        return hostMapper.selectHostInfo(page,hostCondition);
    }

}
