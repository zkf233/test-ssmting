package com.lyl.service.impl;

import com.lyl.pojo.RoleMenu;
import com.lyl.mapper.RoleMenuMapper;
import com.lyl.service.RoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {

}
