package com.lyl.service.impl;

import com.lyl.pojo.MarriedPerson;
import com.lyl.mapper.MarriedPersonMapper;
import com.lyl.service.MarriedPersonService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@Service
public class MarriedPersonServiceImpl extends ServiceImpl<MarriedPersonMapper, MarriedPerson> implements MarriedPersonService {

}
