package com.lyl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lyl.mapper.AdminRoleMapper;
import com.lyl.mapper.RoleMenuMapper;
import com.lyl.pojo.AdminRole;
import com.lyl.pojo.Menu;
import com.lyl.mapper.MenuMapper;
import com.lyl.pojo.RoleMenu;
import com.lyl.pojo.TreeResultJSON;
import com.lyl.service.MenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {
    private AdminRoleMapper adminRoleMapper;// admin-->角色
    private RoleMenuMapper roleMenuMapper;// 角色--》菜单
    private MenuMapper menuMapper;//菜单

    @Autowired
    public void setAdminRoleMapper(AdminRoleMapper adminRoleMapper) {
        this.adminRoleMapper = adminRoleMapper;
    }

    @Autowired
    public void setMenuMapper(MenuMapper menuMapper) {
        this.menuMapper = menuMapper;
    }

    @Autowired
    public void setRoleMenuMapper(RoleMenuMapper roleMenuMapper) {
        this.roleMenuMapper = roleMenuMapper;
    }

    /**
     * 菜单查询
     * @param pid 根据pid查询具体隶属于mid下的菜单信息
     * @param aid admin的id，根据admin查询对应的角色rid，根据rid查询菜单的mid，最后根据mid查询出菜单
     * @return 返回处理后的JSON模板对象列表
     */
    @Override
    public List<TreeResultJSON> selMenuInfo(String pid, Integer aid) {
        // MP并不希望我们再去Mapper层写sql代码，而是通过面向对象思想写，也就是通过Wrapper对象完成SQL的书写

        //## 查询admin的角色id
        // 创建Wrapper条件构造器
        QueryWrapper<AdminRole> adminRoleQueryWrapper = new QueryWrapper<>();
        adminRoleQueryWrapper.eq("aid",aid).select("rid");
        // selectObjs将根据Wrapper中的查询字段名返回该字段的查询结果，如果有多字段名则自动以最终查询sql语句的最前面字段名为返回数据，返回该字段的数据
        List<Object> rids = adminRoleMapper.selectObjs(adminRoleQueryWrapper);

        //## 根据角色查询对应的角色的菜单id
        //select mid from t_role_menu where rid in(1);
        QueryWrapper<RoleMenu>  roleMenuQueryWrapper= new QueryWrapper<>();
        roleMenuQueryWrapper.in("rid",rids).select("mid");
        List<Object> mids = roleMenuMapper.selectObjs(roleMenuQueryWrapper);

        //## 根据菜单的id查询菜单的具体内容
        //select * from t_menu where mid in (select mid from t_role_menu where rid in(1)) and pid=0
        QueryWrapper<Menu>  menuQueryWrapper= new QueryWrapper<>();
        menuQueryWrapper.in("mid",mids).eq("pid",pid);
        List<Menu> menus = menuMapper.selectList(menuQueryWrapper);

        // 将查询出的List<Menu>转换成Lit<TreeResultJSON>即可
        List<TreeResultJSON> list = new ArrayList<>();
        for (Menu menu:menus){
            // 将menu的信息取出存放在TreeResultJSON对象中
            TreeResultJSON resultJSON = new TreeResultJSON();
            // 设置Mid为pid
            // 因为rbac设计菜单的mid和pid互相联系，如果是父级目录，mid则会作为pid传递给下一次的查询菜单方法
            resultJSON.setId(menu.getMid());
            // 设置菜单名
            resultJSON.setText(menu.getMname());
            // t_menu中的isparent属性表示是否为父级目录，而前台JSON模板的state属性为closed的是关闭的表示为目录，而open的是打开表示不是目录而是目录中的元素
            resultJSON.setState("1".equals(menu.getIsparent())?"closed":"open");
            // 异步树中的attributes属性是用户自定义的数据为Map集合
            Map<String,Object> map = new HashMap<>();
            // 判断有没有子节点，没有子节点直接显示资源页面
            map.put("isparent",menu.getIsparent());
            // 将菜单的url传递
            map.put("url",menu.getUrl());
            resultJSON.setAttributes(map);
            // 将resultJSON对象添加到列表中
            list.add(resultJSON);
        }
        return list;
    }
}
