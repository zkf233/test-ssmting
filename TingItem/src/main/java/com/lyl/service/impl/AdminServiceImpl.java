package com.lyl.service.impl;

import com.lyl.pojo.Admin;
import com.lyl.mapper.AdminMapper;
import com.lyl.service.AdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {

}
