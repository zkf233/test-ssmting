package com.lyl.service.impl;

import com.lyl.pojo.Planner;
import com.lyl.mapper.PlannerMapper;
import com.lyl.service.PlannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
@Service
public class PlannerServiceImpl extends ServiceImpl<PlannerMapper, Planner> implements PlannerService {

}
