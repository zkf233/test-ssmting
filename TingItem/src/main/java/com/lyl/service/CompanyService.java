package com.lyl.service;

import com.lyl.pojo.Company;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
public interface CompanyService extends IService<Company> {

}
