package com.lyl.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lyl.pojo.Host;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lyl.pojo.HostCondition;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
public interface HostService extends IService<Host> {
    IPage<Host> findHostInfo(IPage<Host> page, HostCondition hostCondition);
}
