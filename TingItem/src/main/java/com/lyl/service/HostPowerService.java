package com.lyl.service;

import com.lyl.pojo.HostPower;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
public interface HostPowerService extends IService<HostPower> {

}
