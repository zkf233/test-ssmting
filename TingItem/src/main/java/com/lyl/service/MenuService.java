package com.lyl.service;

import com.lyl.pojo.Menu;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lyl.pojo.TreeResultJSON;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Clean
 * @since 2021-08-12
 */
public interface MenuService extends IService<Menu> {
    List<TreeResultJSON> selMenuInfo(String id,Integer aid);
}
