<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    pageContext.setAttribute("basePath",basePath);
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <base href="<%=basePath%>"/>
    <title>登录界面</title>
    <link rel="stylesheet" type="text/css" href="static/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="static/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="static/themes/demo.css">
    <link rel="stylesheet" type="text/css" href="static/themes/color.css">
    <script type="text/javascript" src="static/jquery.min.js"></script>
    <script type="text/javascript" src="static/jquery.easyui.min.js"></script>
    <script type="text/javascript">
        $(function (){
            $("#zh").click(function (){
                var long = $('#sjc').val()
                var dateVar = longForsfm(long)
                console.log(dateVar)
                $('#sj').val(dateVar)
            })
        })
        /*
        时间戳转换时分秒
         */
        function longForsfm(mss) {
            var dateTypeDate = "";
            var date = new Date();
            console.log(mss)
            date.setTime(parseInt(mss));
            dateTypeDate += date.getFullYear(); //年
            dateTypeDate += "-" + (date.getMonth() + 1); //月
            dateTypeDate += "-" + date.getDate(); //日
            dateTypeDate += " "+date.getHours(); //时
            dateTypeDate += ":" + date.getMinutes();  //分
            dateTypeDate += ":" + date.getSeconds();  //分
            console.log("date"+dateTypeDate)
            return dateTypeDate;
        }
    </script>
</head>
<body>
<h2>Hello World!</h2>
输入时间戳<input type="text" id="sjc">
<a href="javascript:void(0)" class="easyui-linkbutton" id="zh" data-options="iconCls:'icon-search'">转换</a>
<input type="text" id="sj">
</body>

</html>
