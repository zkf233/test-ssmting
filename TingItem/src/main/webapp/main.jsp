<%--
  Created by IntelliJ IDEA.
  User: CNDA
  Date: 2021/8/12
  Time: 10:26
  To change this template use File | Settings | File Templates.
--%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="<%=basePath%>"/>
    <title>管理员界面</title>
    <link rel="stylesheet" type="text/css" href="static/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="static/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="static/themes/demo.css">
    <link rel="stylesheet" type="text/css" href="static/themes/color.css">
    <script type="text/javascript" src="static/jquery.min.js"></script>
    <script type="text/javascript" src="static/jquery.easyui.min.js"></script>
    <!-- 实现js代码 -->
    <script type="text/javascript">
        $(function (){
            // 实现点击菜单选项，创建对应的选项卡
            // 实现菜单节点的点击事件
            $('#tt').tree({
                onClick: function(node){
                    // 判断是否打开，如果为closed则发送Ajax请求服务器检索子节点
                    // 如果为open，检查是否有子节点-->children为子节点包含的数组元素的个数，如果没有子节点则为undefined，这样就可以判断是不是目录节点
                    if(node.attributes.isparent!=="1"){
                        // 创建选项卡
                        // 判断选项卡是否重复
                        var flag = $("#tabs").tabs('exists', node.text);
                        if (flag) {
                            // 如果重复，则选择该选项卡
                            $("#tabs").tabs('select', node.text);
                        } else {
                            // 如果不重复就创建新的选项卡
                            $("#tabs").tabs('add', {
                                title: node.text,
                                closable: true,
                                content:"<iframe src='"+node.attributes.url+"' style='border: none;width: 99%;height: 99%'></iframe>"// 让选项卡中的context属性填充iframe显示数据
                            })
                        }
                    }
                }
            });
        })
    </script>
</head>
<body class="easyui-layout">
<!-- 页面顶部 -->

<div data-options="region:'north',border:false" style="height:75px;">
    <!-- 设置顶部背景 -->
    <!-- 使用嵌套布局 -->
    <div class="easyui-layout" data-options="fit:true">
        <div data-options="region:'west',border:false" style="width:30%; background-color: #333333;text-align: center" >
            <!-- 设置logo -->
            <img src="static/img/logo.png" style="display: inline-block; width: 100%; max-width: 100%; height: 90%;margin-top: 5px"/>
        </div>
        <div data-options="region:'center',border:false" style="width:50%;background-color: #333333;text-align: center">
            <!-- 设置网站标题 -->
            <span style="color: white;font-size: 40px;position: relative;top: 10px">Ting&nbsp;&nbsp;域&nbsp;&nbsp;主&nbsp;&nbsp;持&nbsp;&nbsp;人&nbsp;&nbsp;后&nbsp;&nbsp;台&nbsp;&nbsp;管&nbsp;&nbsp;理</span>
        </div>
        <div data-options="region:'east',border:false" style="width:20%;background-color: #333333">
            <!-- 设置登录信息 -->
            <span style="position: relative;top: 50px">
                <span style="color: white">欢迎，${sessionScope.admin.aname}</span>
                <span><a href="#" style="color: white">退出</a></span>
            </span>
        </div>
    </div>
</div>
<!-- 页面底部 -->

<div data-options="region:'south'" style="height:50px;"></div>
<!-- 菜单栏 -->

<div data-options="region:'west',title:'菜单栏',collapsible:false" style="width:150px;">
    <!-- 异步树菜单 -->
    <!-- url：远程请求数据，我们将他指向我们的菜单加载方法中在MenuController的getMenu方法 -->
    <ul id="tt" class="easyui-tree" data-options="url:'menu/getMenu'"></ul>

</div>
<!-- 内容显示 -->

<div data-options="region:'center'" style="padding:5px;background:#eee;">
    <!-- 菜单内容在内容显示区域用选项卡的形式出现 -->
    <div id="tabs" class="easyui-tabs" data-options="fit:true" style="width:100%;height:250px">
        <div title="首页" style="padding:10px"></div>
    </div>
</div>
</body>
</html>
