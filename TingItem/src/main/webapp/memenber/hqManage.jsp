<%--
  Created by IntelliJ IDEA.
  User: CNDA
  Date: 2021/8/16
  Time: 8:56
  To change this template use File | Settings | File Templates.
--%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="<%=basePath%>"/>
    <title></title>
    <link rel="stylesheet" type="text/css" href="static/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="static/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="static/themes/demo.css">
    <link rel="stylesheet" type="text/css" href="static/themes/color.css">
    <script type="text/javascript" src="static/jquery.min.js"></script>
    <script type="text/javascript" src="static/jquery.easyui.min.js"></script>
    <script type="text/javascript">
        /**********************使用js创建dataGrid***************************/
        $(function () {
            $("#dg").datagrid({
                //url: "host/hostInfo",// 远程请求数据的url地址
                pagination: true,// 显示分页工具栏
                pageNumber: 1,// 分页显示的初始化页码
                pageSize: 5, // 一页显示多少条数据
                title: "查询结果",// 设置标题
                pageList: [5, 10, 15],// 在设置分页属性的时候 初始化页面大小选择列表.
                striped: true,// 开启隔行变色（斑马线效果）
                checkOnSelect: false,
                rownumbers: true,// 显示一个行号列。
                columns: // 映射表列的字段名与显示给用户的列名,DataGrid列是一个数组对象，该元素也是一个数组对象。
                // 元素数组里面的元素是一个配置对象，它用来定义每一个列字段。
                    [[
                        {field: 'cname', title: '姓名', width: 100},
                        {field: 'ceo', title: '公司法人', width: 100},
                        {field: 'cmail', title: '公司邮箱', width: 100},
                        {field: 'starttime', title: '注册时间', width: 100},
                        {field: 'ordernumber', title: '订单量', width: 100},
                        {field: 'status', title: '账号状态', width: 100}
                    ]],
                toolbar: "#tb",//定义顶部工具栏
            })
        })
        /**********************条件查询的ajax请求点击事件***************************/
        $(function () {
            $("#cx").bind('click', function () {
                // 获取姓名，权重，星推荐等查询条件数据
                var status = $('#status').val();
                var ordernumber = $('#ordernumber').val();
                var cname = $("input[name='cname']").val();

                // 发送Ajax请求，直接调用dataGrid的底层的ajax请求load，可以提供我们传递自己的参数执行一次查询，通过调用这个方法从服务器加载新数据。
                // 注意dataGrid组件的load方法，一旦一次设置，则生命周期有效
                $("#dg").datagrid('load', {
                    status: status,
                    ordernumber: ordernumber,
                    cname: cname,
                })
            })
        })
    </script>
</head>
<body>
<h1>婚庆公司管理</h1>
<!--  -->
<!-- 婚庆公司管理面板 -->
<div class="easyui-panel" title="婚庆公司管理界面"
     style="width:100%;height:100%;background:#fafafa;">
    <!-- 创建动态查询筛选条件div -->
    <div style="width: 100%;height: 60px;">
        <form action="" style="margin: auto;width: 80%">
            <div style="margin: auto;width: 80%">
                <!-- 公司名称查询：使用easyui的textbox-->
                <label>
                    <input name="cname" class="easyui-textbox" data-options="prompt:'公司名称'" style="width: 200px">
                </label>
                <!-- 账号状态 -->
                <label>
                    <select name="status" data-options="editable:false,value:'账号状态'" id="status" class="easyui-combobox"
                            style="width: 150px">
                        <option value="0">禁用</option>
                        <option value="1">正常</option>
                    </select>
                </label>
                <!-- 订单量的排序，最终结果由sql语句决定 -->
                <label>
                    <select name="ordernumber" data-options="editable:false,value:'订单量排序'" id="ordernumber" class="easyui-combobox"
                            style="width: 150px">
                        <option value="ASC">升序</option>
                        <option value="DESC">降序</option>
                    </select>
                </label>
                <!-- 查询按钮和导出按钮 -->
                <label>
                    <a href="javascript:void(0)" id="cx" class="easyui-linkbutton" data-options="iconCls:'icon-search'"
                       style="margin-right: 20px">查询</a>
                </label>
            </div>
        </form>
    </div>

    <!-- 创建DataGrid显示查询到的数据 -->
    <table id="dg"></table>
</div>
<!-- 定义顶部工具栏，使用标签定义 -->
<div id="tb">
    <!-- 添加主持人 -->
    <a href="javascript:void(0)" id="addHost" class="easyui-linkbutton"
       data-options="iconCls:'icon-add',plain:true,text:'添加主持人'"></a>
    <!-- 权限设置 -->
    <a href="javascript:void(0)" id="roleSet" class="easyui-linkbutton"
       data-options="iconCls:'icon-add',plain:true,text:'权限设置'"></a>
    <!-- 禁用账号 -->
    <a href="javascript:void(0)" id="stopUser" class="easyui-linkbutton"
       data-options="iconCls:'icon-edit',plain:true,text:'账号状态切换'"></a>
    <!-- 批量操作 -->
    <a href="javascript:void(0)" id="batchProcessor" class="easyui-linkbutton"
       data-options="iconCls:'icon-remove',plain:true,text:'批量操作'"></a>
</div>
</body>
</html>
