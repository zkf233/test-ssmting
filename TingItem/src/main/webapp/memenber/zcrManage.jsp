<%--
  Created by IntelliJ IDEA.
  User: CNDA
  Date: 2021/8/16
  Time: 8:43
  To change this template use File | Settings | File Templates.
--%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="<%=basePath%>"/>
    <title></title>
    <link rel="stylesheet" type="text/css" href="static/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="static/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="static/themes/demo.css">
    <link rel="stylesheet" type="text/css" href="static/themes/color.css">
    <script type="text/javascript" src="static/jquery.min.js"></script>
    <script type="text/javascript" src="static/jquery.easyui.min.js"></script>
    <script type="text/javascript">
        // 使用js方式创建dataGrid
        $(function () {
            var IsCheckFlag = true;
            $("#dg").datagrid({
                url: "host/hostInfo",// 远程请求数据的url地址
                pagination: true,// 显示分页工具栏
                pageNumber: 1,// 分页显示的初始化页码
                pageSize: 5, // 一页显示多少条数据
                title: "查询结果",// 设置标题
                pageList: [5, 10, 15],// 在设置分页属性的时候 初始化页面大小选择列表.
                striped: true,// 开启隔行变色（斑马线效果）
                checkOnSelect: false,
                rownumbers: true,// 显示一个行号列。
                columns: // 映射表列的字段名与显示给用户的列名,DataGrid列是一个数组对象，该元素也是一个数组对象。
                // 元素数组里面的元素是一个配置对象，它用来定义每一个列字段。
                    [[
                        {field: 'row', checkbox: true},
                        {
                            field: 'strong', title: '权重', width: 100,
                            formatter: function (value, row, index) {
                                return "<input type='text' value='" + value + "' style='width: 50px;' onblur='changeStrong(this," + row.hid + ")'>";
                            }
                        },
                        {field: 'hname', title: '姓名', width: 100},
                        {field: 'hphone', title: '手机号', width: 150},
                        {
                            field: 'starttime', title: '开通时间', width: 200,
                            formatter: function (value, row, index) {
                                // 将数据格式化
                                var dateTypeDate = "";
                                var date = new Date();
                                date.setTime(parseInt(value));
                                console.log(date)
                                dateTypeDate += date.getFullYear(); //年
                                dateTypeDate += "-" + (date.getMonth() + 1); //月
                                dateTypeDate += "-" + date.getDate(); //日
                                dateTypeDate += " " + date.getHours(); //时
                                dateTypeDate += ":" + date.getMinutes();  //分
                                dateTypeDate += ":" + date.getSeconds();  //分
                                return dateTypeDate;
                            }
                        },
                        {
                            field: 'hpprice', title: '价格', width: 100,
                            formatter: function (value, row, index) {
                                if (row.hostPower != null) {
                                    return row.hostPower.hpprice;
                                } else {
                                    return "";
                                }
                            }
                        },
                        {field: 'ordernumber', title: '订单量', width: 100},
                        {
                            field: 'hpdiscount', title: '折扣', width: 100,
                            formatter: function (value, row, index) {
                                if (row.hostPower != null) {
                                    return row.hostPower.hpdiscount;
                                } else {
                                    return "";
                                }
                            }
                        },
                        {
                            field: 'hpstar', title: '星推荐', width: 100,
                            formatter: function (value, row, index) {
                                if (row.hostPower != null) {
                                    return row.hostPower.hpstar == 1 ? "推荐" : "不推荐";
                                } else {
                                    return "";
                                }

                            }
                        },
                        {
                            field: 'status', title: '帐号状态', width: 100,
                            formatter: function (value, row, index) {
                                if (value != null) {
                                    return value == 1 ? "正常" : "异常";
                                } else {
                                    return "";
                                }
                            }
                        },
                    ]],
                toolbar: "#tb",//定义顶部工具栏
                onClickCell: function (rowIndex, field, value) {
                    IsCheckFlag = false;
                },
                onSelect: function (rowIndex, rowData) {
                    if (!IsCheckFlag) {
                        IsCheckFlag = true;
                        $("#dg").datagrid("unselectRow", rowIndex);
                    }
                },
                onUnselect: function (rowIndex, rowData) {
                    if (!IsCheckFlag) {
                        IsCheckFlag = true;
                        $("#dg").datagrid("selectRow", rowIndex);
                    }
                }
            })


            /** 点击查询按钮根据条件查询
             * 1.获取条件选择的所有条件如果不选，则默认为查询所有
             * 2.将获取的条件传递到后台，后台接收条件，并根据条件查询，最后将查询结果返回到dataGrid显示
             *
             */
            $("#cx").bind('click', function () {
                // 获取姓名，权重，星推荐等查询条件数据
                var status = $('#status').val();
                var strong = $('#strong').val();
                var hpstar = $('#hpstar').val();
                var hpdiscount = $('#hpdiscount').val();
                var hname = $("input[name='hname']").val();

                // 发送Ajax请求，直接调用dataGrid的底层的ajax请求load，可以提供我们传递自己的参数执行一次查询，通过调用这个方法从服务器加载新数据。
                // 注意dataGrid组件的load方法，一旦一次设置，则生命周期有效
                $("#dg").datagrid('load', {
                    status: status,
                    strong: strong,
                    hpstar: hpstar,
                    hpdiscount: hpdiscount,
                    hname: hname,
                })

            })

            /**
             * 导出，使用pio，将数据导出成exc表格并保存到指定目录
             */
            $("#dc").bind('click', function () {
                var hosts = $("#dg").datagrid('getRows');
                console.log(typeof JSON.stringify({"hosts": hosts}));
                $.ajax({
                    dataType: "json",
                    contentType: 'application/json',
                    type: "post",
                    url: "host/export",//
                    data: JSON.stringify({"hosts": hosts}),
                    success: function (data) {
                        window.location.href = "<%=basePath%>host/download?fileName=" + data.text + ".xls";
                    }, error: function (error) {
                        console.log("error:" + error);
                    }
                })
            })

            // 添加主持人按钮的单击事件，弹出对话框
            $("#addHost").click(function () {
                $("#dd").dialog('open');
            })

            // 点击确认按钮实现ajax请求添加主持人
            $("#indeed").click(function () {
                // 获取请求数据发送至后台
                var hname = $('#hname').val()
                var pwd = $('#pwd').val()
                var phone = $('#phone').val()
                if (hname === '' || pwd === '' || phone === '') {
                    return;
                }
                $('#ff').form('submit', {
                    url: "host/addHost",
                    // 响应的数据是来自服务器的原始数据
                    success: function (data) {
                        var data1 = eval('(' + data + ')');
                        if (data1.success) {
                            // 关闭窗口
                            $("#dd").dialog('closed')
                            // 重新加载dataGrid
                            $("#dg").datagrid('reload')
                            $.messager.alert("增加主持人信息", data1.message, "info");
                        } else {
                            $.messager.alert("增加主持人信息", data1.message, "error");
                        }
                    }
                })
            })
            // 取消按钮，实现窗口关闭
            $('#quit').click(function () {
                $('#dd').dialog({'closed': true})
            })

            // 获取表格内所有被选中的行对象
            function selectChecked() {
                return $('#dg').datagrid('getSelections');
            }

            /*
            批量禁用账户
             */
            $('#stopUser').click(function () {
                var rows = selectChecked();
                console.log(rows)
                if (rows.length > 0) {
                    $.ajax({
                        dataType: "json",
                        contentType: 'application/json',
                        type: "post",
                        url: "host/stopUser",//
                        data: JSON.stringify({"hosts": rows}),
                        success: function (data) {
                            console.log(data)
                            if (data.success) {
                                // 重新加载dataGrid
                                $("#dg").datagrid('reload')
                                $.messager.alert("切换账户状态", data.message, "info");
                            } else {
                                $.messager.alert("切换账户状态", data.message, "error");
                            }
                        }
                    })
                } else {
                    $.messager.alert("选择错误", "请选择用户", "info")
                }
            })


        })

        /*
        主持人权限修改
         */
        $(function () {
            // 当权限修改的dialog关闭时重置form表单的数据，防止下一个点击事件form表单没有更新
            $("#prom").dialog({
                onClose: function () {
                    $('#powerFrom').form('reset')
                }
            })
            // 点击事件
            $("#roleSet").click(function () {
                // 去读选中的主持人信息行
                var tr = $('#dg').datagrid("getChecked")
                if (tr.length == 1) {
                    // 数据回显，如果hostPower为空则不需要回显，反之回显数据
                    var hostPower = tr[0].hostPower;
                    console.log(tr)
                    // 将hid一起发送至后台
                    $('#hostid').val(tr[0].hid)
                    if (hostPower != null) {
                        // 回显数据
                        $('#hpid').val(hostPower.hpid)
                        // 是否星推荐回显
                        hostPower.hpstar == "1" ? $('#hpstar_yes').radiobutton({checked: true}) : $('#hpstar_no').radiobutton({checked: true})
                        // 是否自动添加订单回显
                        hostPower.hpOrderPower == "1" ? $('#hp_order_power_yes').radiobutton({checked: true}) : $('#hp_order_power_no').radiobutton({checked: true})
                        // 星推荐开始日期回显
                        $('#hpstar_begindate').datebox('setValue', longFornyr(hostPower.hpstarBegindate));
                        // 星推荐结束日期回显
                        $('#hpstar_enddate').datebox('setValue', longFornyr(hostPower.hpstarEnddate));
                        // 每日星推荐开始回显
                        $('#hpstar_begintime').timespinner('setValue', longForsfm(hostPower.hpstarBegintime));
                        // 每日星推荐结束回显
                        $('#hpstar_endtime').timespinner('setValue', longForsfm(hostPower.hpstarEndtime));
                        // 折扣开始日期回显
                        $('#hp_dis_starttime').datebox('setValue', longFornyr(hostPower.hpDisStarttime));
                        // 折扣结束日期回显
                        $('#hp_dis_endtime').datebox('setValue', longFornyr(hostPower.hpDisEndtime));
                        // 价格回显
                        $('#hpprice').textbox('setValue', hostPower.hpprice)
                        // 折扣回显
                        $('#discount').combobox('select', hostPower.hpdiscount);
                        // 管理费回显
                        $('#hpcosts').textbox('setValue', hostPower.hpcosts)
                    }
                    $("#prom").dialog('open')
                } else if (tr.length > 1) {
                    $.messager.alert("提示", "只能选中一个主持人信息", "info")
                } else {
                    $.messager.alert("提示", "请选中一个主持人信息", "info")
                }
            })
            // 权限修改的确定请求
            $('#pIndeed').click(function () {
                $('#powerFrom').form('submit', {
                    url: "hostPower/power",
                    success: function (data) {
                        var data1 = eval('(' + data + ')');
                        if (data1.success) {
                            $.messager.alert("权限设置", data1.message, "info");
                            // 关闭对话框
                            $('#prom').dialog('close')
                            // 重新加载dataGrid
                            $("#dg").datagrid('reload')
                        } else {
                            $.messager.alert("权限设置", data1.message, "error");
                        }
                    }
                })
            })
        })

        /*
        主持人权限批量操作
        */
        $(function () {
            $("#batchProm").dialog({
                onClose: function () {
                    $('#batchPromFrom').form('reset')
                }
            })
            $('#batchProcessor').click(function (){
                var rows = $('#dg').datagrid('getChecked')
                var hostids = ""
                var pids = ""
                for(var i=0;i<rows.length;i++){
                    hostids+=rows[i].hid+","
                    if(rows[i].hostPower == null){
                        pids+=" ,"
                    }else {
                        pids+=rows[i].hostPower.hpid+","
                    }
                }
                $('#hostids').val(hostids)
                $('#pid').val(pids)
                // 打开批量操作dialog
                if (rows.length>0){
                    $('#batchProm').dialog('open')
                }else {
                    $.messager.alert("提示","请至少选中一个用户","info");
                }
            })
            // 表单提交
            $('#batchIndeed').click(function (){
                $('#batchPromFrom').form('submit',{
                    url:"hostPower/batchHostPower",
                    success:function (data){
                        var data1 = eval('(' + data + ')');
                        if (data1.success) {
                            $.messager.alert("批量权限设置", data1.message, "info");
                            // 关闭对话框
                            $('#batchProm').dialog('close')
                            // 重新加载dataGrid
                            $("#dg").datagrid('reload')
                            // 重置表单内容

                        } else {
                            $.messager.alert("批量权限设置", data1.message, "error");
                        }
                    }
                })
            })

        })

        // 发送ajax请求修改strong
        function changeStrong(inp, hid) {
            $.post("host/strongUp", {hid: hid, strong: inp.value}, function (data) {
                if (data.success) {
                    $("#dg").datagrid('reload');
                    $.messager.alert("修改权重", data.message, "info");
                } else {
                    $.messager.alert("修改权重", data.message, "error");
                }
            })
        }

        /*
        时间戳转换年月日
         */
        function longFornyr(dataLong) {
            var dateTypeDate = "";
            var date = new Date();
            date.setTime(parseInt(dataLong));
            dateTypeDate += date.getFullYear(); //年
            dateTypeDate += "-" + (date.getMonth() + 1); //月
            dateTypeDate += "-" + date.getDate(); //日
            console.log(dateTypeDate)
            return dateTypeDate;

        }

        /*
        时间戳转换时分秒，mysql的数据库的time类型，转成毫秒数返回，我们接受后需要将其格式化
        格式化后得到的是1970-1-1 HH:mm:ss，固定格式不能按正常的一天毫秒数去求，否则结果可能相差甚多
         */
        function longForsfm(mss) {
            var dateTypeDate = "";
            var date = new Date();
            date.setTime(parseInt(mss));
            dateTypeDate += date.getHours(); //时
            dateTypeDate += ":" + date.getMinutes();  //分
            dateTypeDate += ":" + date.getSeconds();  //分
            return dateTypeDate;
        }

        /*
        格式化datebox组件的显示格式
         */
        function myformatter(date) {
            return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        }

        /*
        格式化datebox组件的显示格式
         */
        function myparser(date) {
            var t = Date.parse(date);
            if (!isNaN(t)){
                return new Date(t);
            } else {
                return new Date();
            }
        }

    </script>
</head>
<body>
<h2>主持人管理界面</h2>
<!-- 创建面板 -->
<div class="easyui-panel" title="主持人管理界面"
     style="width:100%;height:100%;background:#fafafa;">
    <!-- 创建动态查询筛选条件div -->
    <div style="width: 100%;height: 60px;">
        <form action="" style="margin: auto;width: 80%">
            <div style="margin: auto;width: 80%">
                <!-- 姓名查询：使用easyui的textbox-->
                <label>
                    <input name="hname" class="easyui-textbox" data-options="prompt:'姓名'" style="width: 100px">
                </label>
                <!-- 账号状态 -->
                <label>
                    <select name="status" data-options="editable:false,value:'账号状态'" id="status" class="easyui-combobox"
                            style="width: 100px">
                        <option value="0">禁用</option>
                        <option value="1">正常</option>
                    </select>
                </label>
                <!-- 权重的排序，最终结果由sql语句决定 -->
                <label>
                    <select name="strong" data-options="editable:false,value:'权重排序'" id="strong" class="easyui-combobox"
                            style="width: 100px">
                        <option value="ASC">升序</option>
                        <option value="DESC">降序</option>
                    </select>
                </label>
                <!-- 星推荐 -->
                <label>
                    <select name="hpstar" data-options="editable:false,value:'星推荐'" id="hpstar" class="easyui-combobox"
                            style="width: 100px">
                        <option value="1">推荐</option>
                        <option value="0">不推荐</option>
                    </select>
                </label>
                <!-- 折扣 -->
                <label>
                    <select name="hpdiscount" data-options="editable:false,value:'折扣'" id="hpdiscount"
                            class="easyui-combobox" style="width: 100px">
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                </label>
                <!-- 查询按钮和导出按钮 -->
                <label>
                    <a href="javascript:void(0)" id="cx" class="easyui-linkbutton" data-options="iconCls:'icon-search'"
                       style="margin-right: 20px">查询</a>
                    <a href="javascript:void(0)" id="dc" class="easyui-linkbutton"
                       data-options="iconCls:'icon-ok'">导出</a>
                </label>
            </div>
        </form>
    </div>

    <!-- 创建DataGrid显示查询到的数据 -->
    <table id="dg"></table>
</div>
<!-- 定义顶部工具栏，使用标签定义 -->
<div id="tb">
    <!-- 添加主持人 -->
    <a href="javascript:void(0)" id="addHost" class="easyui-linkbutton"
       data-options="iconCls:'icon-add',plain:true,text:'添加主持人'"></a>
    <!-- 权限设置 -->
    <a href="javascript:void(0)" id="roleSet" class="easyui-linkbutton"
       data-options="iconCls:'icon-add',plain:true,text:'权限设置'"></a>
    <!-- 禁用账号 -->
    <a href="javascript:void(0)" id="stopUser" class="easyui-linkbutton"
       data-options="iconCls:'icon-edit',plain:true,text:'账号状态切换'"></a>
    <!-- 批量操作 -->
    <a href="javascript:void(0)" id="batchProcessor" class="easyui-linkbutton"
       data-options="iconCls:'icon-remove',plain:true,text:'批量操作'"></a>
</div>

<!-- 添加主持人对话框 -->
<div id="dd" class="easyui-dialog" data-options="title: '添加主持人',closed:true,
                    cache: false,
                    modal: true,
                    iconCls:'icon-save'" style="width: 400px;height: 300px;">
    <div style="margin-top: 20px;height: 80%">
        <!-- ajax请求 -->
        <form id="ff" method="post">
            <div style="margin-bottom: 20px;text-align: center">
                <input name="hname" id="hname" class="easyui-textbox"
                       data-options="label:'姓名:',required:true,validateOnBlur:true,validateOnCreate:false,missingMessage:'请填写用户名！'"
                       style="width: 80%">
            </div>
            <div style="margin-bottom: 20px;text-align: center">
                <input name="hpwd" id="pwd" class="easyui-passwordbox"
                       data-options="label:'密码:',required:true,validateOnBlur:true,validateOnCreate:false,missingMessage:'请填写密码！'"
                       style="width: 80%">
            </div>
            <div style="margin-bottom: 20px;text-align: center">
                <input name="hphone" id="phone" class="easyui-textbox"
                       data-options="label:'联系方式:',required:true,validateOnBlur:true,validateOnCreate:false,missingMessage:'请填写用户手机号码！'"
                       style="width: 80%">
            </div>
        </form>
        <!-- 定义按钮组 -->
        <div style="float: right">
            <!-- 确定添加 -->
            <a href="javascript:void(0)" id="indeed" class="easyui-linkbutton"
               data-options="iconCls:'icon-ok',plain:true,text:'确定保存'"></a>
            <!-- 取消操作 -->
            <a href="javascript:void(0)" id="quit" class="easyui-linkbutton"
               data-options="iconCls:'icon-no',plain:true,text:'取消操作'"></a>
        </div>
    </div>
</div>

<!-- 权限设置对话框
    1.一次修改只能修改一个用户
    2.发送ajax请求
 -->
<div id="prom" class="easyui-dialog" data-options="title: '修改权限',closed:true,
                    cache: false,
                    modal: true,
                    iconCls:'icon-save',
                    left:400,
                    top:100" style="width: 600px;height: 450px;">
    <div style="margin-top: 10px;">
        <!-- ajax请求 -->
        <form id="powerFrom" method="post">
            <table cellpadding="5px" style="margin: auto">
                <tr>
                    <td>
                        <input type="text" name="hpid" id="hpid" style="display: none">
                        <input type="text" name="hostid" id="hostid" style="display: none">
                    </td>
                </tr>
                <tr>
                    <td>是否星推荐：</td>
                    <td>
                        <!-- 是否星推荐（单选） -->
                        <label> <input class="easyui-radiobutton" name="hpstar" id="hpstar_yes" value="1" label="是"
                                       labelPosition="after"></label>
                        <label> <input class="easyui-radiobutton" name="hpstar" id="hpstar_no" value="0" label="否"
                                       checked
                                       labelPosition="after"></label>
                    </td>
                </tr>
                <tr>
                    <td>自填订单：</td>
                    <td><!-- 是否允许自动添加订单（单选） -->

                        <label><input class="easyui-radiobutton" name="hpOrderPower" id="hp_order_power_yes" value="1"
                                      label="是"
                                      labelPosition="after"></label>
                        <label><input class="easyui-radiobutton" name="hpOrderPower" id="hp_order_power_no" value="0"
                                      label="否"
                                      labelPosition="after" checked></label>
                    </td>
                </tr>
                <tr>
                    <td>星推荐日期：</td>
                    <td>
                        <!-- 星推荐开始——结束日期（日期选择） -->
                        <input id="hpstar_begindate" name="hpstarBegindate"
                               data-options="formatter:myformatter,parser:myparser" type="text" class="easyui-datebox"
                               required="required">
                        -
                        <input id="hpstar_enddate" name="hpstarEnddate"
                               data-options="formatter:myformatter,parser:myparser" type="text" class="easyui-datebox"
                               required="required">
                    </td>
                </tr>
                <tr>
                    <td>星推荐时间：</td>
                    <td>
                        <!-- 每日星推荐开始——结束时间（日期选择） -->
                        <input id="hpstar_begintime" name="hpstarBegintime" data-options="showSeconds:true"
                               class="easyui-timespinner"
                               required="required">
                        -
                        <input id="hpstar_endtime" name="hpstarEndtime" data-options="showSeconds:true"
                               class="easyui-timespinner"
                               required="required">
                    </td>
                </tr>
                <tr>
                    <td>折扣值：</td>
                    <td>
                        <!-- 折扣值（下拉列表） -->
                        <select name="hpdiscount" data-options="editable:false,value:'折扣'" id="discount"
                                class="easyui-combobox" style="width: 100px">
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>折扣时间：</td>
                    <td>
                        <!-- 折扣开始——结束时间（日期选择） -->
                        <input id="hp_dis_starttime" name="hpDisStarttime"
                               data-options="formatter:myformatter,parser:myparser" type="text" class="easyui-datebox"
                               required="required">
                        -
                        <input id="hp_dis_endtime" name="hpDisEndtime"
                               data-options="formatter:myformatter,parser:myparser" type="text" class="easyui-datebox"
                               required="required">
                    </td>
                </tr>
                <tr>
                    <td>价格：</td>
                    <td>
                        <!-- 价格（文本） -->
                        <input name="hpprice" id="hpprice" class="easyui-textbox" prompt="请输入价格:" style="width:40%;">
                    </td>
                </tr>
                <tr>
                    <td>管理费：</td>
                    <td>
                        <!-- 管理费（文本）-->
                        <input name="hpcosts" id="hpcosts" class="easyui-textbox" prompt="请输入管理费:" style="width:40%;">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <!-- 确定添加 -->
                        <a href="javascript:void(0)" style="margin: auto" id="pIndeed" class="easyui-linkbutton c1"
                           data-options="plain:true,text:'确定保存'"></a>
                    </td>
                </tr>

            </table>
        </form>
    </div>
</div>
<!--权限批量操作对话框，不需要数据回显
只需要提交表单即可
-->
<div id="batchProm" class="easyui-dialog" data-options="title: '批量修改权限',closed:true,
                    cache: false,
                    modal: true,
                    iconCls:'icon-save',
                    left:400,
                    top:100" style="width: 600px;height: 450px;">
    <div style="margin-top: 10px;">
        <!-- ajax请求 -->
        <form id="batchPromFrom" method="post">
            <table cellpadding="5px" style="margin: auto">
                <tr>
                    <td>
                        <input type="text" name="hostids" id="hostids" style="display: none">
                        <input type="text" name="pids" id="pid" style="display: none">
                    </td>
                </tr>
                <tr>
                    <td>是否星推荐：</td>
                    <td>
                        <!-- 是否星推荐（单选） -->
                        <label> <input class="easyui-radiobutton" name="hpstar" value="1" label="是"
                                       labelPosition="after"></label>
                        <label> <input class="easyui-radiobutton" name="hpstar" value="0" label="否"
                                       checked
                                       labelPosition="after"></label>
                    </td>
                </tr>
                <tr>
                    <td>自填订单：</td>
                    <td><!-- 是否允许自动添加订单（单选） -->

                        <label><input class="easyui-radiobutton" name="hpOrderPower" value="1"
                                      label="是"
                                      labelPosition="after"></label>
                        <label><input class="easyui-radiobutton" name="hpOrderPower" value="0"
                                      label="否"
                                      labelPosition="after" checked></label>
                    </td>
                </tr>
                <tr>
                    <td>星推荐日期：</td>
                    <td>
                        <!-- 星推荐开始——结束日期（日期选择） -->
                        <input name="hpstarBegindate"
                               data-options="formatter:myformatter,parser:myparser" type="text" class="easyui-datebox"
                               required="required">
                        -
                        <input name="hpstarEnddate"
                               data-options="formatter:myformatter,parser:myparser" type="text" class="easyui-datebox"
                               required="required">
                    </td>
                </tr>
                <tr>
                    <td>星推荐时间：</td>
                    <td>
                        <!-- 每日星推荐开始——结束时间（日期选择） -->
                        <input name="hpstarBegintime" data-options="showSeconds:true"
                               class="easyui-timespinner"
                               required="required">
                        -
                        <input name="hpstarEndtime" data-options="showSeconds:true"
                               class="easyui-timespinner"
                               required="required">
                    </td>
                </tr>
                <tr>
                    <td>折扣值：</td>
                    <td>
                        <!-- 折扣值（下拉列表） -->
                        <select name="hpdiscount" data-options="editable:false,value:'折扣'"
                                class="easyui-combobox" style="width: 100px">
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>折扣时间：</td>
                    <td>
                        <!-- 折扣开始——结束时间（日期选择） -->
                        <input name="hpDisStarttime"
                               data-options="formatter:myformatter,parser:myparser" type="text" class="easyui-datebox"
                               required="required">
                        -
                        <input name="hpDisEndtime"
                               data-options="formatter:myformatter,parser:myparser" type="text" class="easyui-datebox"
                               required="required">
                    </td>
                </tr>
                <tr>
                    <td>价格：</td>
                    <td>
                        <!-- 价格（文本） -->
                        <input name="hpprice" class="easyui-textbox" prompt="请输入价格:" style="width:40%;">
                    </td>
                </tr>
                <tr>
                    <td>管理费：</td>
                    <td>
                        <!-- 管理费（文本）-->
                        <input name="hpcosts" class="easyui-textbox" prompt="请输入管理费:" style="width:40%;">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <!-- 确定添加 -->
                        <a href="javascript:void(0)" style="margin: auto" id="batchIndeed" class="easyui-linkbutton c1"
                           data-options="plain:true,text:'确定保存'"></a>
                    </td>
                </tr>

            </table>
        </form>
    </div>
</div>
</body>
</html>
