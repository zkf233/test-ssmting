<%--
  Created by IntelliJ IDEA.
  User: CNDA
  Date: 2021/8/12
  Time: 9:46
  To change this template use File | Settings | File Templates.
--%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    pageContext.setAttribute("basePath",basePath);
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <base href="<%=basePath%>"/>
    <title>登录界面</title>
    <link rel="stylesheet" type="text/css" href="static/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="static/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="static/themes/demo.css">
    <link rel="stylesheet" type="text/css" href="static/themes/color.css">
    <script type="text/javascript" src="static/jquery.min.js"></script>
    <script type="text/javascript" src="static/jquery.easyui.min.js"></script>
    <script type="text/javascript">
        $(function (){
            $("#login").click(function (){
                // 提交表单
                var loginForm = $("#loginForm");
                loginForm.submit();
            })
        })
    </script>
</head>
<body>
    <div style="margin: 100px auto;width: 402px;">
        <c:if test="${sessionScope.flag=='loginNo'}">
            <font size="21px" style="color: red">用户名或密码错误</font>
        </c:if>
        <!-- 每次页面刷新都将flag从Session移除 -->
        <c:remove var="flag" scope="session"/>
        <form id="loginForm" action="admin/userLogin" method="post">
            <div class="easyui-panel" style="width:400px;padding:50px 60px" title="欢迎登录Ting域主持人后台管理系统">
                <div style="margin-bottom:20px">
                    <input class="easyui-textbox" name="username" prompt="请输入用户名" style="width:100%;height:34px;padding:10px;">
                </div>
                <div style="margin-bottom:20px">
                    <input class="easyui-passwordbox" name="password" prompt="请输入密码" style="width:100%;height:34px;padding:10px">
                </div>
                <div style="margin-bottom:20px">
                    <a href="javascript:void(0)" id="login" class="easyui-linkbutton c3" style="width:120px">点击登录</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input class="easyui-checkbox" name="fruit" value="Apple" label="记住密码" labelPosition="after">
                </div>
            </div>
        </form>
    </div>

</body>
</html>