import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.Test;

import java.util.Scanner;

public class TestCode {
    @Test
    public void testCodeDemo() {
        AutoGenerator ag = new AutoGenerator();
        String path = System.getProperty("user.dir");// 动态获取当前项目路径
        //1. 全局配置
        GlobalConfig config = new GlobalConfig();
        /**
         * GlobalConfig主要配置的属性有：
         * -- 是否支持AR模式
         * -- 生成代码结构的根路径
         * -- 文件是否覆盖
         * -- 主键策略
         * -- 生成基本的resultMap
         * -- 生成基本的SQL片段，也就是xml文件中的sql标签，包含了表中的字段名
         */
        // 是否支持AR模式
        config.setActiveRecord(true)
                .setAuthor("Clean")
                // 生成路径
                .setOutputDir(path+"/src/main/java")
                // 文件覆盖
                .setFileOverride(false)
                // 主键策略
                .setIdType(IdType.AUTO)
                // 设置日期格式　　　　　　　　　　
                .setDateType(DateType.ONLY_DATE)
                // 设置生成的service接口的名字的首字母是否为I，默认Service是以I开头的
                .setServiceName("%sService")
                //生成基本的resultMap
                .setBaseResultMap(true)
                //生成基本的SQL片段
                .setBaseColumnList(true);

        //2. 数据源配置
        /**
         * DataSourceConfig的主要属性配置
         * 此处为MP连接数据库读取数据库中表的属性和字段名，帮我们自动生成项目结构和数据库中表对应的实体类
         * -- 数据库类型
         */
        DataSourceConfig dsConfig = new DataSourceConfig();
        // 设置数据库类型
        dsConfig.setDbType(DbType.MYSQL)
                .setDriverName("com.mysql.cj.jdbc.Driver")
                .setUrl("jdbc:mysql://localhost:3306/tingitemdata")
                .setUsername("root")
                .setPassword("root");

        //3. 策略配置globalConfiguration中
        /**
         * StrategyConfig的主要属性配置
         * -- 全局大写命名
         * -- 数据库表映射到实体类的命名策略
         * -- 生成表（要生成实体类的表名，支持多表·11一起生成，以数组的形式填写）
         * -- 设置表的前缀（TablePrefix）
         */
        StrategyConfig stConfig = new StrategyConfig();
        //全局大写命名
        stConfig.setCapitalMode(true)
                // 数据库表映射到实体的命名策略
                .setNaming(NamingStrategy.underline_to_camel)
                // 生成的表, 支持多表一起生成，以数组形式填写
                .setInclude("t_admin","t_admin_role","t_company","t_host"
                        , "t_host_power","t_married_person","t_menu","t_order","t_planner","t_role","t_role_menu")
                // 设置表的前缀
                .setTablePrefix("t_");

        //4. 包名策略配置
        /**
         * PackageConfig的主要属性配置
         * 公共包--Parent
         */
        PackageConfig pkConfig = new PackageConfig();
        pkConfig.setParent("com.lyl")
                .setMapper("mapper")
                .setService("service")
                .setController("controller")
                .setEntity("pojo")
                .setXml("mapper");

        //5. 整合配置
        /**
         * 整合需要的对象有：
         * --> GolobalConfig
         * --> DataSourceConfig
         * --> StrategyConfig
         * --> PackageConfig
         */
        ag.setGlobalConfig(config)
                .setDataSource(dsConfig)
                .setStrategy(stConfig)
                .setPackageInfo(pkConfig);
        //6. 执行
        ag.execute();
        System.out.println("======= 代码生成完毕 ========");
    }

}
