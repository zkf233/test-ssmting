import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lyl.mapper.AdminRoleMapper;
import com.lyl.pojo.AdminRole;
import com.lyl.pojo.Host;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class TestSelect {
    private final ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
    @Test
    public void selectObj(){
        Host host = new Host();

        QueryWrapper<Host> adminRoleQueryWrapper = new QueryWrapper<>();

        // selectObjs将根据Wrapper中的查询字段名返回该字段的查询结果，如果有多字段名则自动以最终查询sql语句的最前面字段名为返回数据，返回该字段的数据
        List<Host> hosts = host.selectList(adminRoleQueryWrapper);
        System.out.println(hosts);
    }
    @Test
    public void demo1(){
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        for (int i = 0; i < list.size(); i++) {
            list.set(i,1);
        }
        System.out.println(list);
    }
}
